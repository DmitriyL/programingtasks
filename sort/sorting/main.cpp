#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std;


void heapify(int* arr, int n, int i){
    int largest = i;
// �������������� ���������� ������� ��� ������
    int l = 2*i + 1; // ����� = 2*i + 1
    int r = 2*i + 2; // ������ = 2*i + 2

 // ���� ����� �������� ������� ������ �����
    if (l < n && arr[l] > arr[largest])
        largest = l;

   // ���� ������ �������� ������� ������, ��� ����� ������� ������� �� ������ ������
    if (r < n && arr[r] > arr[largest])
        largest = r;

    // ���� ����� ������� ������� �� ������
    if (largest != i)
    {
        swap(arr[i], arr[largest]);

// ���������� ����������� � �������� ���� ���������� ���������
        heapify(arr, n, largest);
    }
}

// �������� �������, ����������� ������������� ����������
void HeapSort(int* arr, int n){
  // ���������� ���� (�������������� ������)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

   // ���� �� ������ ��������� �������� �� ����
    for (int i=n-1; i>=0; i--)
    {
        // ���������� ������� ������ � �����
        swap(arr[0], arr[i]);

        // �������� ��������� heapify �� ����������� ����
        heapify(arr, i, 0);
    }
}










int* generatorProstoy(int n, int beg, int en){
    int* arr = new int(n);

    for(int i = 0; i < n; i++){
        arr[i] = beg+rand()%en;
    }

    return arr;
}

int* generatorUnikalniy(int n, int beg, int en){
    int* mas = new int(n);
    int* mas_pos = new int(en-beg+1);

    int ch = beg;
    for(int i = 0; i < en-beg + 1; i++){
        mas_pos[i] = ch;
        ch++;
    }

    for(int i = 0; i < n; i++){
        int pos = rand()%(en-beg+1);
        mas[i] = mas_pos[pos];
        mas_pos[pos] = mas_pos[en-beg+1];
        en--;
    }
    return mas;
}

int* genUnicPosled(int n, int b, int e){
    int *temp, *arr;
    temp = new int[e - b + 1];
    arr = new int[n];
    for (int i = b; i <= e; i++){
        temp[i-b] = i;
    }
    int emax = e - b + 1;
    for (int i = 0; i < n; i++){
        int r = rand() % (emax);
        arr[i] = temp[r];
        temp[r] = temp[emax-1];
        emax--;
    }
    return arr;
}




void bubbleSort(int* mas, int n){
    for(int i = 0; i  < n; i++){
        for(int j = 0; j < n - 1; j++){
            if(mas[j] > mas[j + 1]){
                swap(mas[j], mas[j + 1]);
            }
        }
    }
    //return mas;
}

void insertionSort(int* mas, int n){
    int buffer;
    int i,j;
    for(i = 0; i < n; i++){
        buffer = mas[i];
        for(j = i-1; j >= 0 && buffer < mas[j]; j--){
            mas[j+1] = mas[j];
        }
        mas[j+1] = buffer;
    }
    //return mas;
}

void quickSort(int* mas, int first, int last){
    int i = first, j = last;
    int buffer, comp;
    comp = mas[(first + last) / 2];
    do{
        while(mas[i] < comp && i < last){
            i++;
        }
        while(mas[i] > comp && i > first){
            j--;
        }
        if(i <= j){
            if(mas[i] > mas[j]){
                buffer = mas[i];
                mas[i] = mas[j];
                mas[j] = buffer;
            }
            i++;
            j--;
        }
    }while(i <= j);
    if(first < j)
        quickSort(mas, first, j);
    if(i < last)
        quickSort(mas, i, last);
    //return mas;
}


void heapSort(int* mas, int n){
    for(int i = n; i>1; i--){
        bool flag = false;
        int buff;
        int sh = 0;
        for(int j = 0; j < n/2;){
            int largest = j, left = 2*j+1, right = 2*i+2;
            if(mas[left] > mas[largest]){
                buff = mas[left];
                swap(mas[left], mas[largest]);
                flag = true;
            }
            if(right < i){
                if(mas[right] > mas[largest]){
                    buff = mas[right];
                    swap(mas[right], mas[largest]);
                    flag = true;
                }
            }
            if(flag && j>0){
                j = (j-1)/2;
                sh++;
                flag = false;
            }else{
                j = j+1+sh;
                sh = 0;
            }
        }
        mas[i] = mas[0];
        int largest = 0, left = 2*0+1, right = 2*0+2;
        for(int j = 0; j < n/2;){
            largest = j, left = 2*j+1, right = 2*i+2;
            if(mas[left] > mas[largest]){
                buff = mas[left];
                swap(mas[left], mas[largest]);
                flag = true;
            }
            if(right < i){
                if(mas[right] > mas[largest]){
                    buff = mas[right];
                    swap(mas[right], mas[largest]);
                    flag = true;
                }
            }
        }
        mas[largest] = buff;
    }
}






int main()
{
    /*
    int n, start, stop;
    cin >> n >> start >> stop;

    /*
    int* mas = generatorProstoy(n, start, stop);
    for(int i = 0; i < n; i++){
        cout << mas[i] << ' ';
    }

    cout << endl;

    */
    //int* unikMas = generatorUnikalniy(100, 0, 100);

   // for(int i = 0; i < 100; i++){
   //     cout << unikMas[i] << ' ';
   // }
    /*
    int* arr = generatorUnikalniy(n,start, stop);
    cout << endl;
    int beg, en;
    beg = clock();
    bubbleSort(arr, n);
    en = clock();
    cout << "BubbleSort - " << en - beg << '\n';

    arr = arr = generatorUnikalniy(n,start, stop);;
    beg = clock();
    insertionSort(arr, n);
    en = clock();
    cout << "InsertionSort - " << en - beg << '\n';

    arr = arr = generatorUnikalniy(n,start, stop);;
    beg = clock();
    quickSort(arr, 0, n);
    en = clock();
    cout << "QuickSort - " << en - beg << '\n';
    */
    ofstream fout("output.txt");
    int st, et, time_start, time_promej;
    for(int j = 1000; j < 10000000; j*=10){
        int all = 0;
        cout << "////////////////////////////////////////////\n";
        time_start = clock();
        for(int i = 0; i < 10; i++){
            int* mas = genUnicPosled(j, 0, j);
            st = clock();
            heapSort(mas, j);
            et = clock();
            all += et-st;
            if(i%1 == 0){
                time_promej = clock();
                cout << i << " - complete" << "time - " << (time_promej-time_start)*1.0/CLOCKS_PER_SEC << endl;
            }
        }
        cout << "j - " << j << " , sr time- " << ((all/10)*1.0/CLOCKS_PER_SEC)*100 << endl;
        fout << "j - " << j << " , time- " << ((all/10)*1.0/CLOCKS_PER_SEC)*100 << ", iterations = 10" << endl;
    }



    return 0;

}
