#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

int format_l(string str, int ch){
    for(int i = 0; i < str.size(); i++){
        if(str[i] != ' '){
            return ch - i;
        }
    }
    return 0;
}

int format_r(string str, int ch){
    return ch - str.size();
}


string correntString(string str,int ch){
    string chstr2;
    string str2 = "";
    for(;ch<str.size(); ch++){
        str2+=str[ch];
    }
    return str2;
}

int main()
{
    int k = 0;
    string str;
    vector<string> strings;
    ifstream cin("input.txt");
    ofstream fout;
    fout.open("output.txt");
    while((getline(cin, str)) && str != "0"){
        strings.push_back(str);
    }

    ///////////////////////////////////////
    //����
    for(string str: strings){
        int ch = format_l(str, k);
        if(ch < 0){
            fout << correntString(str, abs(ch));
        }else{
            for(int i = 0; i < ch; i++){
                fout << " ";
            }
            fout << str;
        }
        fout << "\n";
    }
    ///////////////////////////////////////
    //�������
    fout.close();
    fout.open("output1.txt");
    k = 0;
    for(string str: strings){
        int ch = format_l(str, k);
        if(ch < 0){
            fout << correntString(str, abs(ch));
        }else{
            for(int i = 0; i < ch; i++){
                fout << " ";
            }
            fout << str;
        }
        k++;
        fout << "\n";
    }
    ///////////////////////////////////////
    //����� 80
    fout.close();
    fout.open("output2.txt");
    k = 80;
    for(string str: strings){
        int ch = format_r(str, k);
        for(int i = 0; i < ch; i++){
            fout << " ";
        }
        fout << str;
        fout << "\n";
    }
     ///////////////////////////////////////
    //�� ������
    fout.close();
    fout.open("output3.txt");
    k = 0;
    for(string str: strings){
        int ch = format_l(str, k);
        if(ch < 0){
            fout << correntString(str, abs(ch));
        }else{
            for(int i = 0; i < ch; i++){
                fout << " ";
            }
            fout << str;
        }
        fout << "\n";
    }
    return 0;
}
